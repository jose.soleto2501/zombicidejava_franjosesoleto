package arma;

import java.util.ArrayList;

public class Arma {
    ArrayList<Arma> arma;
    // Atributos de las armas
    private String nombre;
    private int daño;
    private int alcance;
    private int acierto;

    //Constructor 
    public Arma (String nombre,int daño,int alcance,int acierto){
        setNombre(nombre);
        setDaño(daño);
        setAlcance(alcance);
        setAlcance(acierto);
    }
    public Arma (){
        setNombre("Daga");
        setDaño(1);
        setAlcance(1);
        setAcierto(4);

    }

    //Metodo

    public void habilidadEspecialArma(){
        System.out.println("No hay habilidad especial");
    }




    // Getter y Setters de las armas
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setDaño(int daño) {
        this.daño = daño;
    }

    public int getDaño() {
        return daño;
    }

    public void setAlcance(int alcance) {
        this.alcance = alcance;
    }

    public int getAlcance() {
        return alcance;
    }

    public void setAcierto(int acierto) {
        this.acierto = acierto;
    }

    public int getAcierto() {
        return acierto;
    }

}
