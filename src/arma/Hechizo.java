package arma;

public class Hechizo extends Arma {
    public Hechizo(){
        super("Hechizo",1,3,4);
    }

    @Override
    public void habilidadEspecialArma(){
        System.out.println("Mata gratis a 2 caminantes.");
    } 
}
