package arma;

public class Espada extends Arma {

    public Espada(){
        super("Espada",1,1,4);
    }

    @Override
    public void habilidadEspecialArma(){
        System.out.println("Mata gratis a 2 zombies aleatorios.");
    }

}
