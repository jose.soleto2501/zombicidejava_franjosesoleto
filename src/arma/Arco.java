package arma;

public class Arco extends Arma {

    public Arco(){
        super("Arco",1,2,3);
    }

    @Override
    public void habilidadEspecialArma(){
        System.out.println("Mata gratis a 1 corredor.");
    }

}
