package zombies;

import humanoide.Zombie;

public class Gordo extends Zombie{
    
    public Gordo () {
        super("Gordo",2,1,1,"Gordo");
    }

    @Override
    public void habilidadEspecial(){
        System.out.println("Esta es la habilidad especial de gordo");
    }
}

