package zombies;

import humanoide.Zombie;

public class Corredor extends Zombie {
    public Corredor (){
   super("Corredor",1,2,1,"Corredor");
    }

    //MÉTODOS DE LA CLASE CORREDOR
    @Override
    public void habilidadEspecial(){
        System.out.println("Esta es la habilidad de corredor");
    }
}
