package zombies;

import humanoide.Zombie;

public class Caminante extends Zombie{
    public Caminante (){
        super("Caminante",1,1,1,"Caminante");
    }
    @Override
    public void habilidadEspecial(){
        System.out.println("Esta es la habilidad especial de caminante");
    }

}
