package zombicideJava;

import arma.Arma;
import humanoide.Jugador;
import humanoide.Zombie;
import zombies.*;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Partida {
   private ArrayList <Jugador> jugadoresSeleccionados;
   private int nivelMax;

    private int nivel;
    private int ronda;
    private ArrayList<Zombie> zombiesPartida;

    public Partida (ArrayList<Jugador> jugadoresSeleccionados,int nivel){
            setJugadoresSeleccionados(jugadoresSeleccionados);
            setNivel(nivel);
            setNivelMax(10);
            setRonda(1);
            setZombiesPartida(new ArrayList<Zombie>());
    }

    public void nuevaPartida(){
        zombiesPartida=generarZombies();
        boolean opcionJugadores=true;
        //Ataque jugadores
        while (opcionJugadores==true){
            menuJugadores();
        }
        //Ataque zombies


    }

    public void menuJugadores() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Todos tus jugadores han sido seleccionados");
        System.out.println("|----- NIVEL: "+nivel+" - "+ronda+"-----|");
        for (int i = 0; i < jugadoresSeleccionados.size(); i++) {
            System.out.print("==| ");
            for (int j = 0; j < zombiesPartida.size(); j++) {
                Zombie zombieSeleccionado;
                zombieSeleccionado=zombiesPartida.get(j);
                System.out.print(" "+zombieSeleccionado.getNombre());
            }
            System.out.print(" |== ");
            System.out.println(" ");
            Jugador jugadorOpcion;
            jugadorOpcion=jugadoresSeleccionados.get(i);
            Arma armaJugador = jugadorOpcion.getArma();
            System.out.println("JUGADOR: "+jugadorOpcion.getNombre() + " S:"+jugadorOpcion.getSalud()+"ARMA[ "+armaJugador.getNombre()+" DAÑO: "+armaJugador.getDaño()+" DIST: "+ armaJugador.getAlcance()+ " ACIE: "+armaJugador.getAcierto()+"]");
            System.out.println("1- ATACAR");
            System.out.println("2- HABILIDAD ESPECIAL");
            System.out.println("3- BUSCAR");
            System.out.println("4- CAMBIAR ARMA");
            System.out.println("0- PASAR");
            System.out.println("ELIGE una opcion");
            int opcion = sc.nextInt();
            while(opcion!=0){
                switch (opcion) {
                    case 1:
                       zombiesPartida= jugadorOpcion.atacar(zombiesPartida);
                        break;
                    case 2:
                        if (jugadorOpcion.isHabilidadEspecial()){
                            armaJugador.habilidadEspecialArma();
                        } else {
                            System.out.println("Ya has usado la habilidadEspecial");
                        }
                        break;
                    case 3:
                        jugadorOpcion.buscar();
                        break;
                    case 4:
                        jugadorOpcion.cambiarArma();
                        break;
                    default:
                        System.out.println("Ese comando no existe, vuelve a elegir");
                        opcion=sc.nextInt();
                }
            }
        }
    }
        public ArrayList<Zombie> generarZombies() {
        ArrayList<Zombie> zombiesGenerados = new ArrayList<>();
        Random random = new Random();

        int cantidadZombies = nivel;

        for (int i = 0; i < cantidadZombies; i++) {
            int tipoZombie = random.nextInt(3);
            Zombie zombie;

            switch (tipoZombie) {
                case 0:
                    zombie = new Caminante();
                    break;
                case 1:
                    zombie = new Gordo();
                    break;
                case 2:
                    zombie = new Corredor();
                    break;
                default:
                    //Por defecto se crean caminantes
                    zombie = new Caminante();
                    break;
            }

            zombiesGenerados.add(zombie);
        }

        return zombiesGenerados;
    }

    public void comandAtacar() {

    }
    
    public void comandBuscar() {
    }
    public void comandHB() {
    }

    public void comandCambiarArma() {
    }



    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }
    public ArrayList<Jugador> getJugadoresSeleccionados() {
        return jugadoresSeleccionados;
    }

    public void setJugadoresSeleccionados(ArrayList<Jugador> jugadoresSeleccionados) {
        this.jugadoresSeleccionados = jugadoresSeleccionados;
    }
    public int getNivelMax() {
        return nivelMax;
    }

    public void setNivelMax(int nivelMax) {
        this.nivelMax = nivelMax;
    }
    public int getRonda() {
        return ronda;
    }

    public void setRonda(int ronda) {
        this.ronda = ronda;
    }
    public ArrayList<Zombie> getZombiesPartida() {
        return zombiesPartida;
    }

    public void setZombiesPartida(ArrayList<Zombie> zombiesPartida) {
        this.zombiesPartida = zombiesPartida;
    }

}
