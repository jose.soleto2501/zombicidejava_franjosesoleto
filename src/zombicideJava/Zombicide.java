package zombicideJava;


import arma.*;
import humanoide.Jugador;
import humanoide.Zombie;
import zombies.Caminante;
import zombies.Corredor;
import zombies.Gordo;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.function.DoubleToIntFunction;

public class Zombicide {
    private static ArrayList <Jugador> jugadores;
    private static ArrayList <Zombie> zombies;
    private static ArrayList <Arma> armas;

    public static void main(String[] args) {
        jugadores = new ArrayList<Jugador>();
        armas = new ArrayList<Arma>();
        zombies=new ArrayList<Zombie>();
        initPersonajes();
        initZombies();
        initArmas();        
        int opcion = nuevoMenu();

        while (opcion != 0) {
            switch (opcion) {
                case 1:
                    nuevaPartida();
                    break;
                case 2:
                    nuevoPersonaje();
                    break;
                default:
                    System.out.println("Ese comando no existe, vuelve a elegir");
            }
            opcion = nuevoMenu();
        }
        System.out.println("GAME OVER");
    }


    private static int nuevoMenu() {
        Scanner leer = new Scanner(System.in);
        System.out.println();
        System.out.println("|-|-ZOMBICIDE-|-|");
        System.out.println("1- Nueva Partida");
        System.out.println("2- Nuevo Personaje");
        System.out.println("0- Fin de Juego");
        System.out.println("|---------------|");
        int opcion = leer.nextInt();
        return opcion;
    
    }
    private static void nuevoPersonaje() {
        if (jugadores.size()==10){
            System.out.println("El máximo de jugadores es 10");
        } else {
            Scanner sc = new Scanner(System.in);
            String nombreNuevoJugador;
            System.out.println("Introduce el nombre del nuevo personaje");
            nombreNuevoJugador = sc.nextLine();
            Jugador nuevoJugador = new Jugador(nombreNuevoJugador, 5, new Arma());
            jugadores.add(nuevoJugador);
        }

    }
    
    private static void nuevaPartida() {
        while (true) {
            Scanner sc = new Scanner(System.in);
            ArrayList<Jugador> jugadoresSeleccionados = new ArrayList<Jugador>();
            if (jugadores.size() > 3) {
                System.out.println("Cuántos jugadores quieres escoger?");
                int cantidadJugadores = sc.nextInt();
                while (jugadoresSeleccionados.size() != cantidadJugadores) {
                    System.out.println("Selecciona los jugadores");
                    Jugador jugadorSeleccionado;
                    for (int i = 0; i < jugadores.size(); i++) {
                        jugadorSeleccionado = jugadores.get(i);
                        System.out.println(i + "- " + jugadorSeleccionado.getNombre());
                    }
                    System.out.println("Opcion: ");
                    int opcion = sc.nextInt();
                    jugadoresSeleccionados.add(jugadores.get(opcion));
                }
                System.out.println("Has seleccionado los siguientes personajes:");
                for (Jugador personaje : jugadoresSeleccionados) {
                    System.out.println("- " + personaje.getNombre());
                }
                Partida nuevoJuego = new Partida(jugadoresSeleccionados, jugadoresSeleccionados.size());
                nuevoJuego.nuevaPartida();
            } else {
                //Enviamos los 3 jugadores creados por defecto
                Partida nuevoJuego = new Partida(jugadores, jugadores.size());
                nuevoJuego.nuevaPartida();
            }
        }
    }

    private static void initPersonajes(){
        Jugador james = new Jugador("James",7,new Mandoble());
        Jugador marie = new Jugador("Marie",5,new Arma());
        Jugador jaci = new Jugador("Jaci",5,new Arma());
        jugadores.add(james);
        jugadores.add(marie);
        jugadores.add(jaci);

    }

    private static void initArmas() {
        Arco arco = new Arco();
        Hacha hacha = new Hacha();
        Espada espada = new Espada();
        Hechizo hechizo = new Hechizo();
        armas.add(arco);
        armas.add(hacha);
        armas.add(espada);
        armas.add(hechizo);
        
    }


    private static void initZombies() {
       Corredor corredor = new Corredor();
       Caminante caminante = new Caminante();
       Gordo gordo = new Gordo();
       zombies.add(corredor);
       zombies.add(caminante);
       zombies.add(gordo);
    }

}

