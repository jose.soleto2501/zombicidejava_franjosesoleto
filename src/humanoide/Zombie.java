package humanoide;

public class Zombie extends Humanoide{
    //ATRIBUTOS DE LA CLAS ZOMBIE QUE EXTIENDE DE LA CLASE HUMANOIDE
    private int movimiento;
    private int daño;
    private String tipoZombie;


    public Zombie(String nombre, int saludMax, int movimiento, int daño, String tipoZombie ) {
        super(nombre,saludMax);
        setMovimiento(movimiento);
        setDaño(daño);
        setTipoZombie(tipoZombie);

    }

public void ataqueZombie(){
    System.out.println("Este es el ataque zombie uuuu");
}

    //GETTERS Y SETTERS DE LA CLASE ZOMBIE
    public int getMovimiento() {
        return movimiento;
    }
    public void setMovimiento(int movimiento) {
        this.movimiento = movimiento;
    }
    public int getDaño() {
        return daño;
    }
    public void setDaño(int daño) {
        this.daño = daño;
    }
    public String getTipoZombie() {
        return tipoZombie;
    }
    public void setTipoZombie(String tipoZombie) {
        this.tipoZombie = tipoZombie;
    }

    public void habilidadEspecial(){
        System.out.println("Esta es la habilidad especial en común");
    }

}