package humanoide;

import java.util.Random;
import arma.Arma;
import java.util.ArrayList;
import java.util.Scanner;


public class Jugador extends Humanoide {

    private  Arma arma;
    //Atributos de los jugadores
    private boolean pasarRonda;

    private ArrayList<Arma> inventario;
    private boolean habilidadEspecial;


    public Jugador(String nombre,int saludMax,Arma arma) {
        super(nombre,saludMax);
        setArma(arma);
        setPasarRonda(true);
        setHabilidadEspecial(true);
        setInventario(new ArrayList<Arma>());
    }


    public ArrayList<Zombie> atacar(ArrayList<Zombie> zombiesAtacados){
        if (getArma()==null){
            System.out.println("No lleva arma encima");
        } else {
            Random rand = new Random();
            int random = rand.nextInt(zombiesAtacados.size());
            Zombie zombie = zombiesAtacados.get(random);
        }
        return zombiesAtacados;
    }
    public void buscar() {

    }

    public void cambiarArma() {
        Scanner sc = new Scanner(System.in);
        // Mostrar el inventario
        System.out.println("Inventario de " + getNombre() + ":");
        for (int i = 0; i < inventario.size(); i++) {
            System.out.println((i + 1) + ". " + inventario.get(i).getNombre());
        }

        System.out.print("Elige el número de arma que quieres equipar: ");
        int opcion = sc.nextInt();
        if (opcion >= 1 && opcion <= inventario.size()) {
            arma = inventario.get(opcion - 1);
            System.out.println(getNombre() + " ha equipado " + arma.getNombre());
        } else {
            System.out.println("Opción inválida.");
        }
    }
    public void setPasarRonda(boolean pasarRonda) {
        this.pasarRonda = pasarRonda;
    }
    public boolean getPasarRonda(){
        return pasarRonda;
    }

    public void setArma(Arma arma) {
        this.arma = arma;
    }

    public Arma getArma() {
        return arma;
    }
    public ArrayList<Arma> getInventario() {
        return inventario;
    }

    public void setInventario(ArrayList<Arma> inventario) {
        this.inventario = inventario;
    }
    public boolean isHabilidadEspecial() {
        return habilidadEspecial;
    }

    public void setHabilidadEspecial(boolean habilidadEspecial) {
        this.habilidadEspecial = habilidadEspecial;
    }

}


