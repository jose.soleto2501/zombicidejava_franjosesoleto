package humanoide;

import arma.Arma;

public class Humanoide{
    //ATRIBUTOS DE LA CLASE HUMANOIDE
    private String nombre;
    private boolean estaVivo;
    private int salud;
    private int saludMax;

    public Humanoide(String nombre,int saludMax) {
        setNombre(nombre);
        setSalud(saludMax);
        setSaludMax(saludMax);
        setEstaVivo(true);

    }

    //GETTERS Y SETTERS DE LA CLASE HUMANOIDE
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public int getSalud() {
        return salud;
    }
    public void setSalud(int salud) {
        this.salud = salud;
    }
    public int getSaludMax() {
        return saludMax;
    }
    public void setSaludMax(int saludMax) {
        this.saludMax = saludMax;
    }
    public boolean getEstaVivo() {
        return estaVivo;
    }
    public void setEstaVivo(boolean estaVivo) {
        this.estaVivo = estaVivo;
    }

}
